import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit,OnDestroy {
	private req:any;
	homeImageList = [
              // {image:"assets/images/nature/4.jpg", name:"image 4", slug:'video-1'},  
              // {image:"assets/images/nature/5.jpg", name:"image 5", link:'video-1'}, 
              // {image:"assets/images/nature/6.jpg", name:"image 6", link:'video-1'}, 
              // {image:"assets/images/nature/1.jpg", title:"image 1", link:'/videos/video-1'} 

	]
  constructor(private http:Http, private router:Router) {}

  ngOnInit() {
  	this.req = this.http.get('assets/json/videos.json').subscribe(data=>{
      console.log(data.json());
      data.json().filter(item=>{
      	if(item.featured){
      		this.homeImageList.push(item)
      	}
      })
      // this.homeImageList = data.json();
    })
  }

  ngOnDestroy(){
  	this.req.unsubscribe()
  }

  preventNormal(event:MouseEvent, image:any){
  	if (!image.prevented){
  		event.preventDefault()
  	this.router.navigate(['./videos'])
  	
  	}
  	

  }

}

console.log("home.......start");
