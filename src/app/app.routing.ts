import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { DoctorListComponent } from './doctor-list/doctor-list.component';
import { VideoListComponent } from './video-list/video-list.component';
import { PostListComponent } from './post-list/post-list.component';


const appRoutes: Routes = [
{
	path:"",
	component: HomeComponent,	
},

{ 
	path:"doctor",
	component:  DoctorListComponent,	
},
{
	path:"videos",
	component: VideoListComponent,	
},
{
	path:"post",
	component: PostListComponent,	
},
]

@NgModule({
	imports:[
	RouterModule.forRoot(
		appRoutes
		)
	],
	exports: [
	RouterModule
	]
})

export class AppRoutingModule{}