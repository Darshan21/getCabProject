import { Component, OnInit, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Http } from '@angular/http';

@Component({
  selector: 'video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit, OnDestroy {
  private req:any;
	title = "VIDEOS";
  someItem = "<h1>video</h1>"
  todayDate;
  public start = 0;
  public end = 5;
 public videoList:[any];

  constructor( private http:Http,private sanitizer: DomSanitizer) { }
  // constructor( private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.todayDate = new Date()
    this.req = this.http.get('http://rishikesh67.pythonanywhere.com/hygull/api/videos/').subscribe(data=>{
     console.log('video-list.......',this.req);
      console.log(data.json());
      this.videoList = data.json() as [any];
      console.log("+++++++++++++++",this.videoList)
    })
  }

  ngOnDestroy(){
    this.req.unsubscribe()

  }

  doInfinite(): Promise<any> {
    console.log('Begin async operation');
      console.log(this.videoList);
    return new Promise((resolve) => {
      setTimeout(() => {
            
        for (var i = 0; i <5 ; i++) {
          this.start  += 1;
          this.end +=1; 
          console.log(",,,,,,,,,,,,,",this.start);
          this.videoList.push();
          console.log(this.videoList.length);

        }

        console.log('Async operation has ended');
        resolve();
        
      },200);
    })
  }
   getEmbedUrl(item){
    return this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + item.url +'')
  }

}

console.log("video-list..........start");
