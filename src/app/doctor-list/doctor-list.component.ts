//our root app component
import {Component} from '@angular/core';
import { InfiniteScroll } from 'angular2-infinite-scroll';
import {Http} from '@angular/http';
import 'rxjs/Rx';
// import * as config from '../config';

@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.css']
})
export class DoctorListComponent  {
 public posts = [];
 public start = 0;
 public end  = 10;
  throttle = 300;
  scrollDistance = 2;
  // title = 'This is Angular InfiniteScroll v' + systemConfig.map['ngx-infinite-scroll'].split('@')[1];

  constructor(private http:Http) {
     
     http.get('https://jsonplaceholder.typicode.com/comments')
    .flatMap((data) => data.json())
    .subscribe((data)=>{
      this.posts.push(data);
     
    });
     // this.addItems(0,this.end);
    console.log("get...");
     
  }
  
  addItems(startIndex, endIndex) {
    for (let i = 0; i < this.end; ++i) {
      
      this.posts.push([i, ' ', this.generateWord()].join(''));
    

      console.log("add........",i);
    }
  }
  onScrollDown () {
    console.log('scrolled!!');

    // add another 20 items
     this.start += 1;
    this.end += 90;
    if(this.end>=500){
      this.end = 0;
    }
    this.addItems(this.start, this.end);
    // console.log("onscrollDown", this.addItems);
  }
  
  generateWord() {
    return "apple";

  }
}
